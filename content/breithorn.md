---
weight: 1
images:
- /images/breithorn-1.jpg
- /images/breithorn-2.jpg
title: Breithorn
date: 2023-04-18
tags:
- stuff
- landscape
- breithorn
- mountain
- switzerland
---

Peak of the famous Breithorn as seen from Wengen, Switzerland. Everyone should visit this part of the world if only to gaze upon the amazing mountain views.
