---
images:
- /images/about.jpg
title: about
url: about
hideTitle: true
hideExif: true
hideDate: true
---

<div align="center">
	<p>
        Hey, my name is Patrick William. German born and Australian grown, I am a devops engineer by profession and hobbyist photographer at other times.
	</p>
	<p>
		Simply going through life <strong>one photo at a time</strong>.
	</p>
</div>
