---
weight: 1
images:
- /images/the-ocean-window.jpg
title: The Ocean Window
date: 2023-04-18
tags:
- stuff
- landscape
---

View out to Lake Geneva from the dungeons of Château de Chillon.

In the 16th Century, a monk by the name of François de Bonivard spent 6 years in these cold damp conditions. The chains which held him are still there to this day.
