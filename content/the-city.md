---
weight: 1
images:
- /images/the-city.jpg
title: The City
date: 2023-04-18
tags:
- stuff
- landscape
- lausanne
---

View of Lausanne from the top of Lausanne Cathedral