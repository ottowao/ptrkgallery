---
weight: 1
images:
- /images/mountain-cloud.jpg
title: Mountain Clouds
date: 2023-04-18
tags:
- stuff
- landscape
- mountain
- switzerland
---

A more moody Swiss countryside compared to the typical one might see