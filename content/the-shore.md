---
weight: 1
images:
- /images/the-shore.jpg
title: The Shore
date: 2023-04-18
tags:
- stuff
- landscape
- lake-geneva
---

The view north from Lake Geneva
