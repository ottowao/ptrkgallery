---
weight: 1
images:
- /images/pigeon-closeup.jpg
title: Pigeon Closeup
date: 2023-04-18
tags:
- stuff
- animals
---

I took this during a six month work trip to Switzerland from inside my apartment at the time. I was 'working from home' at the time thanks to COVID-19.
