---
weight: 1
images:
- /images/oeschinensee-1.jpg
- /images/oeschinensee-2.jpg
- /images/oeschinensee-3.jpg
title: Oeschinensee
date: 2023-04-18
tags:
- landscape
- oeschinensee
- switzerland
---

It was quite the hike to get up here but the water is a spectacular site
