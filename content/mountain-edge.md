---
weight: 1
images:
- /images/mountain-edge-1.jpg
- /images/mountain-edge-2.jpg
title: Mountain Edge
date: 2023-04-18
tags:
- stuff
- landscape
- mountain
- switzerland
---

Clouds kissing the mountain side